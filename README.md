# README #

Study Send is an iPhone app used to manage the workflow of running a very specific kind of diary study. Work is ongoing to expand Study Send to make it useful for a broader set of diary study applications.

Study Send was created as an academic research tool and is open-sourced in the spirit of expanding scientific understanding.

Version 0.0.0.1

### What is this repository for? ###

A researcher can use Study Send to keep track of a list of anonymized diary study participants and send them messages asking them to complete surveys at specific intervals over a period of time.

For each listed participant, Study Send keeps a phone number or email address, a URL for an online survey (e.g., Qualtrics), and a list of dates and times when they will be asked to complete the survey. Whenever it's time to send a survey to a participant, the researcher gets a notification from Study Send. He or she can then open the app, verify the email or text message containing the survey URL that Study Send has composed, and send it off to the participant.

### How do I get set up? ###

1. Download and install Xcode
1. Clone the study_send repository
1. Open Study Send.xcodeproj with Xcode
1. Command+R to build and run

### How is this licensed? ###

Source code is licensed under the MIT License. See the LICENSE file.

Icons are courtesy of Icons8 https//icons8.com/ and are used through a Creative Commons Attribution-NoDerivs 3.0 Unported license https://creativecommons.org/licenses/by-nd/3.0/

### Who do I talk to? ###

Will Ellis wiellis@vt.edu