//
//  UINavigationController+Settings.h
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Settings)

-(void)setManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;

@end
