//
//  UINavigationController+Settings.m
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "UINavigationController+Settings.h"

@implementation UINavigationController (Settings)

-(void)setManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
{
    if ([[self.viewControllers firstObject] respondsToSelector:@selector(setManagedObjectContext:)]) {
        [[self.viewControllers firstObject] setManagedObjectContext:managedObjectContext];
    }
}

@end
