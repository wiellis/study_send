//
//  NotificationController.h
//  Study Send
//
//  Created by William Ellis on 4/1/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Participant+Utilities.h"

FOUNDATION_EXPORT NSString * const NotificationControllerParticipantUUID_Key;

@interface NotificationController : NSObject

- (instancetype)init;
- (void)scheduleNextAlertForParticipant:(Participant*)participant;
- (void)cancelAlertsForParticipant:(Participant*)participant;

+ (void)clearAppNotifications;
+ (void)configureAppNotifications;

@end
