//
//  NotificationController.m
//  Study Send
//
//  Created by William Ellis on 4/1/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationController.h"
#import "Participant+Utilities.h"
#import "SendDate+Utilities.h"

NSString * const NotificationControllerParticipantUUID_Key = @"participantUUID";

@interface NotificationController ()

@end

@implementation NotificationController

+ (void)configureAppNotifications
{
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
}

- (instancetype)init
{
    return [super init];
}

- (void)scheduleNextAlertForParticipant:(Participant*)participant
{
    // Find and cancel current notification if it exists
    UILocalNotification * currentNotificiation = [self currentNotificationForParticipant:participant];
    if (currentNotificiation) {
        [[UIApplication sharedApplication] cancelLocalNotification:currentNotificiation];
    }
    
    // Find earliest not-yet-sent send date for participant and schedule a notification
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"sent == %@", [NSNumber numberWithInteger:SendDateEnum_Sent_NO]];
    SendDate * sendDate = (SendDate*)([NSMutableArray arrayWithArray:[[participant.sendDates filteredSetUsingPredicate:predicate] sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]]]].firstObject);
    UILocalNotification * notification = [[UILocalNotification alloc] init];
    notification.fireDate = sendDate.date;
    notification.timeZone = [NSTimeZone localTimeZone];
    notification.alertBody = @"Time to Send Study!";
    notification.alertTitle = @"Study Send!";
    notification.applicationIconBadgeNumber = 1;
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.userInfo = @{NotificationControllerParticipantUUID_Key : participant.uuid};
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

-(UILocalNotification*)currentNotificationForParticipant:(Participant*)participant
{
    NSArray * scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger notificationIdx = [scheduledNotifications indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        *stop = [[((UILocalNotification*)obj).userInfo objectForKey:NotificationControllerParticipantUUID_Key] isEqualToString:participant.uuid];
        return *stop;
    }];
    UILocalNotification * currentNotification = (notificationIdx != NSNotFound ? scheduledNotifications[notificationIdx] : nil);
    return currentNotification;
}

- (void)cancelAlertsForParticipant:(Participant*)participant
{
    // Find and cancel current notification if it exists
    UILocalNotification * currentNotificiation = [self currentNotificationForParticipant:participant];
    if (currentNotificiation) {
        [[UIApplication sharedApplication] cancelLocalNotification:currentNotificiation];
    }
}

+ (void)clearAppNotifications;
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

@end
