//
//  ParticipantDetailsTVC.h
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Participant+Utilities.h"

@interface ParticipantDetailsTVC : UITableViewController <UITextFieldDelegate>

@property (strong, nonatomic) Participant * participant;

@end
