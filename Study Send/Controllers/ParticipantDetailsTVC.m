//
//  ParticipantDetailsTVC.m
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "ParticipantDetailsTVC.h"
#import "SendDate+Utilities.h"
#import "NotificationController.h"

// TODO: Make this a setting
const int SEC_IN_TWO_DAYS = 60*60*24*2;

@interface ParticipantDetailsTVC ()

@property (strong, nonatomic) NSManagedObjectContext * managedObjectContext;
@property (strong, nonatomic) NSMutableArray * sendDates;
@property (strong, nonatomic) NotificationController * notificationController;

@property (strong, nonatomic) NSIndexPath * selectedDateIndexPath;
@property (strong, nonatomic) NSIndexPath * datePickerIndexPath;

@end

@implementation ParticipantDetailsTVC

-(NSManagedObjectContext*)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = self.participant.managedObjectContext;
    }
    return _managedObjectContext;
}

-(NSMutableArray*)sendDates
{
    if (!_sendDates)
    {
        // Get sendDates
        _sendDates = [NSMutableArray arrayWithArray:[self.participant.sendDates sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]]]];
    }
    return _sendDates;
}

-(NotificationController*)notificationController {
    if (!_notificationController) {
        _notificationController = [[NotificationController alloc] init];
    }
    return _notificationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addSendDate)];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Core Data

-(void)addSendDate
{
    // TODO: Not guaranteed that the last date is the furthest out
    NSDate * nextDateToSend = [NSDate dateWithTimeInterval:(SEC_IN_TWO_DAYS) sinceDate:([self.sendDates lastObject] ? ((SendDate*)self.sendDates.lastObject).date : [NSDate date])];
    SendDate * sendDate = [SendDate sendDateWithContext:self.managedObjectContext WithDate:nextDateToSend WithParticipant:self.participant];
    //[self.managedObjectContext insertObject:participant];
    [self.sendDates addObject:sendDate];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(self.participant.sendDates.count-1) inSection:[self datesSection]]] withRowAnimation:UITableViewRowAnimationBottom];
    [self.notificationController scheduleNextAlertForParticipant:self.participant];
    [self saveContext];
}

-(void)removeSendDateAtRow:(NSUInteger)row
{
    SendDate * sendDateToDelete = [self.sendDates objectAtIndex:row];
    [self.sendDates removeObjectAtIndex:row];
    [self.managedObjectContext deleteObject:sendDateToDelete];
    sendDateToDelete = nil;
    [self.notificationController scheduleNextAlertForParticipant:self.participant];
    [self saveContext];
}

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [self coreDataAlertWithMessage:@"An error occurred while saving."];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void)coreDataAlertWithMessage:(NSString*)message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)valueChangedForDatePicker:(UIDatePicker *)sender {
    if (self.selectedDateIndexPath && self.datePickerIndexPath) {
        NSUInteger sendDateIdx = self.selectedDateIndexPath.row;
        if (self.selectedDateIndexPath.row > self.datePickerIndexPath.row) {
            //Probably should not end up here
            --sendDateIdx;
        }
        SendDate * sendDate = (SendDate*)[self.sendDates objectAtIndex:sendDateIdx];
        sendDate.date = sender.date;
        [self.tableView reloadRowsAtIndexPaths:@[self.selectedDateIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.notificationController scheduleNextAlertForParticipant:self.participant];
        [self saveContext];
    }
}


#pragma mark - Table view data source and helpers

- (NSInteger)detailsSection
{
    return 0;
}

- (NSInteger)datesSection
{
    return 1;
}

- (NSInteger)studyCodeRow
{
    return 0;
}

- (NSIndexPath*)studyCodeIndexPath
{
    return [NSIndexPath indexPathForRow:[self studyCodeRow] inSection:[self detailsSection]];
}

- (NSInteger)contactRow
{
    return 1;
}

-(NSIndexPath*)contactIndexPath
{
    return [NSIndexPath indexPathForRow:[self contactRow] inSection:[self detailsSection]];
}

- (NSInteger)surveyRow
{
    return 2;
}

-(NSIndexPath*)surveyIndexPath
{
    return [NSIndexPath indexPathForRow:[self surveyRow] inSection:[self detailsSection]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == [self detailsSection]) {
        return @"Participant Details";
    }
    else {
        return @"Send Dates";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == [self detailsSection]) {
        return 3;
    }
    else {
        return (self.datePickerIndexPath ? self.sendDates.count+1 : self.sendDates.count);
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == [self detailsSection])
    {
        if (indexPath.row == [self studyCodeRow])
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"studyCodeCell" forIndexPath:indexPath];
            UITextField * studyCodeTF = (UITextField*)[cell viewWithTag:1];
            [studyCodeTF setText:self.participant.studyCode];
            [studyCodeTF setDelegate:self];
            return cell;
        }
        else if (indexPath.row == [self contactRow])
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
            UITextField * contactTF = (UITextField*)[cell viewWithTag:1];
            [contactTF setText:self.participant.contact];
            [contactTF setDelegate:self];
            return cell;
        }
        else // if (indexPath.row == [self surveyRow])
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"surveyCell" forIndexPath:indexPath];
            UITextField * surveyTF = (UITextField*)[cell viewWithTag:1];
            [surveyTF setText:self.participant.surveyURL];
            [surveyTF setDelegate:self];
            return cell;
        }
    }
    else // if (indexPath.section == [self datesSection])
    {
        if (self.datePickerIndexPath && [self.datePickerIndexPath isEqual:indexPath])
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"datePickerCell" forIndexPath:indexPath];
            UIDatePicker * datePicker = (UIDatePicker*)[cell viewWithTag:1];
            if (!self.selectedDateIndexPath) {
                NSLog(@"DatePicker Error");
            }
            SendDate * sendDate = nil;
            if (indexPath.row < self.selectedDateIndexPath.row) {
                // Probably should never happen
                sendDate = (SendDate*)[self.sendDates objectAtIndex:(self.selectedDateIndexPath.row-1)];
            }
            else {
                sendDate = (SendDate*)[self.sendDates objectAtIndex:self.selectedDateIndexPath.row];
            }
            datePicker.date = sendDate.date;
            return cell;
        }
        else
        {
            SendDate * sendDate = nil;
            if (self.datePickerIndexPath && self.datePickerIndexPath.row < indexPath.row) {
                sendDate = (SendDate*)[self.sendDates objectAtIndex:(indexPath.row-1)];
            }
            else {
                sendDate = (SendDate*)[self.sendDates objectAtIndex:indexPath.row];
            }
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dateCell" forIndexPath:indexPath];
            UILabel * dateLabel = (UILabel*)[cell viewWithTag:1];
            dateLabel.text = [NSDateFormatter localizedStringFromDate:sendDate.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
            UILabel * statusLabel = (UILabel*)[cell viewWithTag:2];
            if ([sendDate.sent integerValue] == SendDateEnum_Sent_YES){
                statusLabel.text = @"Sent";
            }
            else if ([sendDate.sent integerValue] == SendDateEnum_Sent_FAILED){
                statusLabel.text = @"Failed";
            }
            else if ([sendDate.date timeIntervalSinceNow] > 0) {
                statusLabel.text = @"Upcoming";
            }
            else {
                statusLabel.text = @"Due";
            }
            return cell;
        }
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == [self datesSection])
    {
        if (self.selectedDateIndexPath && [indexPath isEqual:self.selectedDateIndexPath]) {
            return NO;
        }
        else if (self.datePickerIndexPath && [indexPath isEqual:self.datePickerIndexPath]) {
            return NO;
        }
        else {
            return YES;
        }
    }
    else {
        return NO;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == [self datesSection]) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            // Delete the row from the data source
            [self removeSendDateAtRow:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == [self datesSection])
    {
        [self.tableView beginUpdates];
        if (self.selectedDateIndexPath == nil)
        {
            self.selectedDateIndexPath = indexPath;
            [self addDatePickerAtIndexPath:[NSIndexPath indexPathForRow:(indexPath.row+1) inSection:indexPath.section]];
        }
        else if ([self.selectedDateIndexPath isEqual:indexPath])
        {
            self.selectedDateIndexPath = nil;
            [self removeDatePickerRow];
        }
        else
        {
            // Remove the date picker row and correct selectedDateIndexPath accordingly
            [self removeDatePickerRow];
            if (indexPath.row <= self.selectedDateIndexPath.row) {
                self.selectedDateIndexPath = indexPath;
            }
            else {
                self.selectedDateIndexPath = [NSIndexPath indexPathForRow:(indexPath.row-1) inSection:indexPath.section];
            }
            [self addDatePickerAtIndexPath:[NSIndexPath indexPathForRow:(self.selectedDateIndexPath.row+1) inSection:indexPath.section]];
        }
        [self.tableView endUpdates];
    }
}


- (void)addDatePickerAtIndexPath:(NSIndexPath*)indexPath
{
    self.datePickerIndexPath = indexPath;
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)removeDatePickerRow
{
    if (self.datePickerIndexPath)
    {
        NSIndexPath * pathToDelete = self.datePickerIndexPath;
        self.datePickerIndexPath = nil;
        [self.tableView deleteRowsAtIndexPaths:@[pathToDelete] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell * cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
        if (indexPath && [indexPath isEqual:[self studyCodeIndexPath]])
        {
            if (textField.text.length >= 1) {
                self.participant.studyCode = textField.text;
                [self saveContext];
            }
            else {
                textField.text = self.participant.studyCode;
            }
        }
        else if (indexPath && [indexPath isEqual:[self contactIndexPath]])
        {
            self.participant.contact = textField.text;
            [self saveContext];
        }
        else if (indexPath && [indexPath isEqual:[self surveyIndexPath]]) {
            self.participant.surveyURL = textField.text;
            [self saveContext];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
