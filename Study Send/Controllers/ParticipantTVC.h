//
//  ParticipantTVC.h
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParticipantTVC : UITableViewController

@property (strong, nonatomic, readonly) NSManagedObjectContext * managedObjectContext;

@end
