//
//  ParticipantTVC.m
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "ParticipantTVC.h"
#import "Participant+Utilities.h"
#import "ParticipantDetailsTVC.h"
#import "AppDelegate.h"
#import "NotificationController.h"

// TODO: Make this a setting
const int NUM_DEFAULT_SEND_DATES = 7;

@interface ParticipantTVC ()

@property (strong, nonatomic, readwrite) NSManagedObjectContext * managedObjectContext;
@property (strong, nonatomic) NSMutableArray * participants;
@property (strong, nonatomic) NotificationController * notificationController;

@end

@implementation ParticipantTVC

-(NSManagedObjectContext*)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    }
    return _managedObjectContext;
}

-(NSMutableArray*)participants
{
    if (!_participants)
    {
        // Get participants
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Participant class])];
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:YES]]];
        NSError *error;
        _participants = [NSMutableArray arrayWithArray:[self.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
        if (error) {
            _participants = [NSMutableArray array];
            [self coreDataAlertWithMessage:@"A problem occurred while loading."];
            NSLog(@"ParticipantTVC viewDidLoad error %@, %@", error, [error userInfo]);
        }
    }
    return _participants;
}

-(NotificationController*)notificationController {
    if (!_notificationController) {
        _notificationController = [[NotificationController alloc] init];
    }
    return _notificationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addParticipantUsingAlert)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addParticipantUsingAlert
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"New Participant" message:@"Enter study code." preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
    {
         textField.placeholder = NSLocalizedString(@"StudyCodePlaceholder", @"Study Code");
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(alertTextFieldDidChange:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:textField];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UITextFieldTextDidChangeNotification
                                                      object:nil];
        NSLog(@"Cancel action");
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        UITextField *studyCodeTF = alertController.textFields.firstObject;
        NSString * studyCodeText = (studyCodeTF.text ? studyCodeTF.text : @"");
        [self addParticipantWithStudyCode:studyCodeText];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UITextFieldTextDidChangeNotification
                                                      object:nil];
        NSLog(@"OK action");
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    okAction.enabled = NO;
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)alertTextFieldDidChange:(NSNotification *)notification
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *contactTF = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = contactTF.text.length > 1;
    }
}

#pragma mark - Core Data

-(void)addParticipantWithStudyCode:(NSString*)studyCode
{
    Participant * participant = [Participant participantWithContext:self.managedObjectContext WithStudyCode:studyCode WithNumDefaultSendDates:NUM_DEFAULT_SEND_DATES];
    //[self.managedObjectContext insertObject:participant];
    [self.participants addObject:participant];
    [self.notificationController scheduleNextAlertForParticipant:participant];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(self.participants.count-1) inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [self saveContext];
}

-(void)removeParticipantAtRow:(NSUInteger)row
{
    Participant * participantToDelete = [self.participants objectAtIndex:row];
    [self.notificationController cancelAlertsForParticipant:participantToDelete];
    [self.participants removeObjectAtIndex:row];
    [self.managedObjectContext deleteObject:participantToDelete];
    participantToDelete = nil;
    [self saveContext];
}

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [self coreDataAlertWithMessage:@"An error occurred while saving."];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void)coreDataAlertWithMessage:(NSString*)message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Participants";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.participants.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Participant * participant = (Participant*)[self.participants objectAtIndex:indexPath.row];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"participantCell" forIndexPath:indexPath];
    UILabel * studyCodeLabel = (UILabel*)[cell viewWithTag:1];
    studyCodeLabel.text = [participant.studyCode copy];
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self removeParticipantAtRow:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[ParticipantDetailsTVC class]])
    {
        ParticipantDetailsTVC * destinationPDTVC = (ParticipantDetailsTVC*)[segue destinationViewController];
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            destinationPDTVC.participant = (Participant*)[self.participants objectAtIndex:[self.tableView indexPathForCell:(UITableViewCell*)sender].row];
        }
    }
}

@end
