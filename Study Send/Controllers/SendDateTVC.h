//
//  SendDateTVC.h
//  Study Send
//
//  Created by William Ellis on 3/29/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MessageUI;

@interface SendDateTVC : UITableViewController <MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic, readonly) NSManagedObjectContext * managedObjectContext;

@end
