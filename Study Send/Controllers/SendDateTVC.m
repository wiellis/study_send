//
//  SendDateTVC.m
//  Study Send
//
//  Created by William Ellis on 3/29/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "SendDateTVC.h"
#import "SendDate+Utilities.h"
#import "AppDelegate.h"
#import "Participant+Utilities.h"
#import "NotificationController.h"

@interface SendDateTVC ()

@property (strong, nonatomic, readwrite) NSManagedObjectContext * managedObjectContext;
@property (strong, nonatomic) NSMutableArray * sendDates;
@property (strong, nonatomic) NSIndexPath * selectedDateIndexPath;

@property (strong, nonatomic) NotificationController * notificationController;

@end

@implementation SendDateTVC

-(NSManagedObjectContext*)managedObjectContext
{
    if (!_managedObjectContext) {
        _managedObjectContext = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    }
    return _managedObjectContext;
}

-(NSMutableArray*)sendDates
{
    if (!_sendDates)
    {
        // Get sendDates
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([SendDate class])];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"sent != %@", [NSNumber numberWithInteger:SendDateEnum_Sent_YES]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]]];
        NSError *error;
        _sendDates = [NSMutableArray arrayWithArray:[self.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
        if (error) {
            _sendDates = [NSMutableArray array];
            [self coreDataAlertWithMessage:@"A problem occurred while loading."];
            NSLog(@"ParticipantTVC viewDidLoad error %@, %@", error, [error userInfo]);
        }
    }
    return _sendDates;
}

-(NotificationController*)notificationController {
    if (!_notificationController) {
        _notificationController = [[NotificationController alloc] init];
    }
    return _notificationController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setSendDates:nil];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Core Data

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            [self coreDataAlertWithMessage:@"An error occurred while saving."];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void)coreDataAlertWithMessage:(NSString*)message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"OK action");
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - MFMessageComposeViewController and delegate

- (void)sendSMS:(NSString *)bodyOfMessage recipientList:(NSArray *)recipients
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = bodyOfMessage;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    SendDate * sendDate = (SendDate*)[self.sendDates objectAtIndex:self.selectedDateIndexPath.row];
    if (result == MessageComposeResultCancelled) {
        NSLog(@"Message cancelled");
    }
    else if (result == MessageComposeResultSent) {
        sendDate.sent = [NSNumber numberWithInteger:SendDateEnum_Sent_YES];
        [self.notificationController scheduleNextAlertForParticipant:sendDate.participant];
        [self saveContext];
        [NotificationController clearAppNotifications];
        NSLog(@"Message sent");
    }
    else {
        sendDate.sent = [NSNumber numberWithInteger:SendDateEnum_Sent_FAILED];
        [self.notificationController scheduleNextAlertForParticipant:sendDate.participant];
        [self saveContext];
        [NotificationController clearAppNotifications];
        NSLog(@"Message failed");
    }

    self.selectedDateIndexPath = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMailComposeViewController and delegate
- (void)sendEmailWithSubject:(NSString*)subject WithBody:(NSString *)body WithRecipients:(NSArray *)recipients
{
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail])
    {
        [controller setSubject:subject];
        [controller setMessageBody:body isHTML:NO];
        [controller setToRecipients:recipients];
        controller.mailComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    SendDate * sendDate = (SendDate*)[self.sendDates objectAtIndex:self.selectedDateIndexPath.row];
    if (result == MFMailComposeResultCancelled || result == MFMailComposeResultSaved) {
        NSLog(@"Mail cancelled");
    }
    else if (result == MFMailComposeResultSent) {
        sendDate.sent = [NSNumber numberWithInteger:SendDateEnum_Sent_YES];
        [self.notificationController scheduleNextAlertForParticipant:sendDate.participant];
        [self saveContext];
        [NotificationController clearAppNotifications];
        NSLog(@"Mail sent");
    }
    else {
        sendDate.sent = [NSNumber numberWithInteger:SendDateEnum_Sent_FAILED];
        [self.notificationController scheduleNextAlertForParticipant:sendDate.participant];
        [self saveContext];
        [NotificationController clearAppNotifications];
        NSLog(@"Message failed");
    }
    self.selectedDateIndexPath = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Participants";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sendDates.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SendDate * sendDate = (SendDate*)[self.sendDates objectAtIndex:indexPath.row];
    Participant * participant = sendDate.participant;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];
    UILabel * studyCodeLabel = (UILabel*)[cell viewWithTag:1];
    studyCodeLabel.text = [participant.studyCode copy];
    UILabel * sendDateLabel = (UILabel*)[cell viewWithTag:2];
    sendDateLabel.text = [NSDateFormatter localizedStringFromDate:sendDate.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
    UILabel * statusLabel = (UILabel*)[cell viewWithTag:3];
    if ([sendDate.sent integerValue] == SendDateEnum_Sent_YES){
        statusLabel.text = @"Sent";
    }
    else if ([sendDate.sent integerValue] == SendDateEnum_Sent_FAILED){
        statusLabel.text = @"Failed";
    }
    else if ([sendDate.date timeIntervalSinceNow] > 0) {
        statusLabel.text = @"Upcoming";
    }
    else {
        statusLabel.text = @"Due";
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * messageStub = @"Thank you for participating in our study. Please answer the following questionnaire: ";
    // TODO: Make this a setting
    NSString * messageSubject = @"Diary study";
    SendDate * sendDate = (SendDate*)[self.sendDates objectAtIndex:indexPath.row];
    Participant * participant = sendDate.participant;
    if (![sendDate.sent boolValue])
    {
        if ([sendDate.date timeIntervalSinceNow] <= 0) {
            NSString * message = [NSString stringWithFormat:@"%@%@", messageStub, participant.surveyURL];
            if ([participant contactIsPhoneNumber]) {
                self.selectedDateIndexPath = indexPath;
                [self sendSMS:message recipientList:@[participant.contact]];
            }
            else {
                self.selectedDateIndexPath = indexPath;
                [self sendEmailWithSubject:messageSubject WithBody:message WithRecipients:@[participant.contact]];
            }
        }
        else {
            
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
