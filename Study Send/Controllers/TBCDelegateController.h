//
//  TBCDelegateController.h
//  Study Send
//
//  Created by William Ellis on 3/29/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TBCDelegateController : NSObject <UITabBarControllerDelegate>

@property (strong, nonatomic) UITabBarController * tabBarController;

@end
