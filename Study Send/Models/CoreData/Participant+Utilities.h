//
//  Participant+Utilities.h
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "Participant.h"

@interface Participant (Utilities)

+ (Participant*) participantWithContext:(NSManagedObjectContext*)context WithStudyCode:(NSString*)contact;
+ (Participant*) participantWithContext:(NSManagedObjectContext*)context WithStudyCode:(NSString*)contact WithNumDefaultSendDates:(NSUInteger)numDefaultSendDates;

-(BOOL)contactIsPhoneNumber;

@end
