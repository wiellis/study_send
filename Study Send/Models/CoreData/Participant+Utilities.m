//
//  Participant+Utilities.m
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "Participant+Utilities.h"
#import "SendDate+Utilities.h"

@implementation Participant (Utilities)

+ (Participant*) participantWithContext:(NSManagedObjectContext*)context WithStudyCode:(NSString*)studyCode
{
    Participant * participant = [[Participant alloc] initWithEntity:[NSEntityDescription entityForName:NSStringFromClass([Participant class]) inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    participant.createdAt = [NSDate date];
    participant.studyCode = studyCode;
    participant.uuid = [[NSUUID UUID] UUIDString];
    return participant;
}

#define ONE_DAY_IN_SECONDS (60*60*24)
#define TWO_DAYS_IN_SECONDS (60*60*24*2)
+ (Participant*) participantWithContext:(NSManagedObjectContext*)context WithStudyCode:(NSString*)studyCode WithNumDefaultSendDates:(NSUInteger)numDefaultSendDates
{
    Participant * participant = [Participant participantWithContext:context WithStudyCode:studyCode];
    NSDate * date = [NSDate dateWithTimeIntervalSinceNow:ONE_DAY_IN_SECONDS];  // Two days later
    for (NSUInteger idx = 0; idx < numDefaultSendDates; ++idx)
    {
        [SendDate sendDateWithContext:context WithDate:date WithParticipant:participant];
        date = [date dateByAddingTimeInterval:TWO_DAYS_IN_SECONDS];
    }
    return participant;
}

-(BOOL)contactIsPhoneNumber
{
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];

    if (!self.contact)
    {
        return NO;
    }
    else
    {
        NSTextCheckingResult * match = [detector firstMatchInString:self.contact options:kNilOptions range:NSMakeRange(0, [self.contact length])];
        //NSString *phoneNumber = [match phoneNumber];
        if (match && (match.range.location == 0))
            return YES;
        else
            return NO;
    }
}


@end
