//
//  Participant.h
//  Study Send
//
//  Created by William Ellis on 4/1/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SendDate;

@interface Participant : NSManagedObject

@property (nonatomic, retain) NSString * contact;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * studyCode;
@property (nonatomic, retain) NSString * surveyURL;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSSet *sendDates;
@end

@interface Participant (CoreDataGeneratedAccessors)

- (void)addSendDatesObject:(SendDate *)value;
- (void)removeSendDatesObject:(SendDate *)value;
- (void)addSendDates:(NSSet *)values;
- (void)removeSendDates:(NSSet *)values;

@end
