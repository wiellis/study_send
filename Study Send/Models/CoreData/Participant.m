//
//  Participant.m
//  Study Send
//
//  Created by William Ellis on 4/1/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "Participant.h"
#import "SendDate.h"


@implementation Participant

@dynamic contact;
@dynamic createdAt;
@dynamic studyCode;
@dynamic surveyURL;
@dynamic uuid;
@dynamic sendDates;

@end
