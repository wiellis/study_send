//
//  SendDate+Utilities.h
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "SendDate.h"

typedef enum : NSInteger {
    SendDateEnum_Sent_NO,
    SendDateEnum_Sent_YES,
    SendDateEnum_Sent_FAILED
} SendDateEnum_Sent;

@interface SendDate (Utilities)

+ (SendDate*) sendDateWithContext:(NSManagedObjectContext*)context WithDate:(NSDate*)date WithParticipant:(Participant*)participant;

@end
