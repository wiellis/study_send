//
//  SendDate+Utilities.m
//  Study Send
//
//  Created by William Ellis on 3/28/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "SendDate+Utilities.h"

@implementation SendDate (Utilities)

+ (SendDate*) sendDateWithContext:(NSManagedObjectContext*)context WithDate:(NSDate*)date WithParticipant:(Participant*)participant
{
    SendDate * sendDate = [[SendDate alloc] initWithEntity:[NSEntityDescription entityForName:NSStringFromClass([SendDate class]) inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    sendDate.date = date;
    sendDate.participant = participant;
    return sendDate;
}


@end
