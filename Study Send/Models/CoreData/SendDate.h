//
//  SendDate.h
//  Study Send
//
//  Created by William Ellis on 3/29/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Participant;

@interface SendDate : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * sent;
@property (nonatomic, retain) Participant *participant;

@end
