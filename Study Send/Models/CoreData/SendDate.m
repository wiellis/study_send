//
//  SendDate.m
//  Study Send
//
//  Created by William Ellis on 3/29/15.
//  Copyright (c) 2015 William Ellis. All rights reserved.
//

#import "SendDate.h"
#import "Participant.h"


@implementation SendDate

@dynamic date;
@dynamic sent;
@dynamic participant;

@end
